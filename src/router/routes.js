
const routes = [
  {
    path: "/",
    redirect: {
      name: "listofUsers",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "listofUsers",
        name: "listofUsers",
        component: () => import("pages/ListofUsers.vue"),
      },
      {
        path: "addUser",
        name: "addUser",
        component: () => import("pages/AddUserPage.vue"),
      },
    ],
  },
]

export default routes
